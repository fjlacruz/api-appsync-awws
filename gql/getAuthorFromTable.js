import { util } from "@aws-appsync/utils";

export function request(ctx) {
    return dynamoDBGetItemRequest({ PK: ctx.arguments.PK, SK: ctx.arguments.SK }); // Pasa tanto PK como SK a la función helper
}

export function response(ctx) {
    return ctx.result;
}

/**
 * A helper function to get a DynamoDB item
 */
function dynamoDBGetItemRequest(key) {
    return {
        operation: "GetItem",
        key: util.dynamodb.toMapValues(key),
    };
}
