import { util } from "@aws-appsync/utils";

export function request(ctx) {
  const PK = `AUTHOR#${util.autoId()}`;
  const { ...values } = ctx.args;
  values.ups = 1;
  values.downs = 0;
  values.version = 1;
  return { payload: { key: { PK }, values: values } };
}

export function response(ctx) {
  return ctx.result;
}
