import { util } from "@aws-appsync/utils";

export function request(ctx) {
  const { key, values } = ctx.prev.result;
  console.log('************************ createPostItem ************************************')
  console.log('************************ key ************************************')
  console.log(key)
  console.log('************************ values ************************************')
  console.log(values.input)
  
  const val= {
    "SK": `AUTHOR#${values.input.SK}`,
    "GSI1PK": "fdfdf",
    "GSI1SK": "dfdf",
    "Nombre": values.input.Nombre,
    "Apellidos": values.input.Apellidos,
    "rut": values.input.rut,
    "FechaNacimiento": values.input.FechaNacimiento
}


  return {
    operation: "PutItem",
    key: util.dynamodb.toMapValues(key),
    attributeValues: util.dynamodb.toMapValues(val),
  };
}

export function response(ctx) {
  return ctx.result;
}
