import { ListTablesCommand, DynamoDBClient, PutItemCommand, GetItemCommand  } from '@aws-sdk/client-dynamodb';
import { QueryCommand, DynamoDBDocumentClient } from '@aws-sdk/lib-dynamodb';

const dynamoDBClient = new DynamoDBClient({ region: "us-east-1" }); 

export const lambdaHandler = async (event, _) => {
  console.log("Got an Invoke Request.");
  console.log(`EVENT: ${JSON.stringify(event, null, 2)}`);

  const name = !event.arguments?.name ? "world" : event.arguments.name;
  let PK_= event.arguments.PK  ;
  //let SK_ =event.SK;

  switch (event.field) {
    case "lambdaCall":
      return `Hello, ${name}`;
    case "sayGoodbye":
      return `Bye, ${name}`;
    case "getDataFromDynamoDB":
      return getDataFromDynamoDB(PK_);
      //return event;
    default:
      throw new Error("Unknown field, unable to resolve " + event.field);
  }
};

const getDataFromDynamoDB = async (PK_) => {
  try {
    const params = {
      TableName: "Base-Collections",
      KeyConditionExpression: "#PK=:PK and #SK=:SK", 
      ExpressionAttributeNames: { "#PK": "PK", "#SK": "SK"},
      ExpressionAttributeValues: {
        ":PK": `AUTHOR#${PK_}`,
        ":SK": `AUTHOR#`
      }
    };
    const command = new QueryCommand(params);
    const response = await dynamoDBClient.send(command);
     let data= response.Items[0]
    if (!data) {
      return "No se encontraron datos para la clave proporcionada";
    }
    return data;
  } catch (error) {
    console.error("Error al consultar DynamoDB: ", error);
    throw new Error("Error al consultar DynamoDB");
  }
};
